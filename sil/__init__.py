__name__ = 'sil'
major = 0
minor = 1
patch = 6
__version__ = '{}.{}.{}'.format(major, minor, patch)
from .sil import Sil
